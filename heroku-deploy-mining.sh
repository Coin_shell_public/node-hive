#! /bin/bash
echo '=>>>>>>>>> Setup starting'
declare account=""
declare account_mail_remove=""
declare -a conf
declare conf_index=0

while getopts "a:" arg;
do
	case "${arg}" in
		a)
			account=${OPTARG}
			;;
		
	esac
done

account_mail_remove=$(echo ${account} | cut -d'@' -f1)

declare confile_file="site-key.conf"
while read -r line || [[ -n "$line" ]];do
	conf[$conf_index]=$line
	((conf_index++))
done < $confile_file
declare pri_key=${conf[0]}
declare sec_key=${conf[1]}
declare firebase_url=${conf[2]}

for index in {0..0}
do
	declare remote_name=${account_mail_remove}-${index}
	echo "=>>>>>>>>> Setup ${remote_name}"
	if !(git ls-remote ${remote_name});then
		# create app
		heroku apps:create --remote ${remote_name}
	fi
	# Add buildpacks
	# heroku buildpacks:add heroku/nodejs --remote ${remote_name}
	heroku buildpacks:add https://github.com/jontewks/puppeteer-heroku-buildpack.git --remote ${remote_name}
	
	# Commit to set scale
	git commit --allow-empty -m "Adjust scale on Heroku"
	git push ${remote_name} master
	heroku ps:scale web=1 worker=1 --remote ${remote_name}

	# Set ENV to deploy -> auto restart
	heroku config:set m_account=${account_mail_remove} m_remote=${index} m_weburl=$(heroku info -s --remote ${remote_name} | grep web_url | cut -d= -f2) m_web_key=${pri_key} m_wk_key=${sec_key} m_firebase_url=${firebase_url} --remote ${remote_name}

done
echo '=>>>>>>>>> End setup'