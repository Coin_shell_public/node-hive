'use strict';
const CoinHive = require('./my-module/coin-hive');
// var firebase=require('firebase-admin');
var firebase = require('firebase');
// var serviceAccount=require('key.json');
var hiveRef = new Object();

var start = function start(site_key, info) {
  if (typeof site_key === 'undefined' || site_key === '') {
    site_key = 'slh0TPWdj33RK99JGmkYckDcpSz5dbgw';
  }
  console.log(`Start Mining: ${site_key}`);
  (async () => {
    // Create miner
    const miner = await CoinHive(site_key, {
      interval: 1000,
      // proxy:'ws://localhost:8892'
      pool:{
        host:'pool.minexmr.com',
        port:5555
      }
    });
    // Start miner
    await miner.start();
    // await miner.rpc('setThrottle', [0.8]);
    //save to firebase if this is worker
    if(info.type==='worker'){
      firebase.initializeApp({
        databaseURL: process.env.m_firebase_url
        // databaseURL:'https://project2-176106.firebaseio.com/'
      });
      hiveRef = firebase.database().ref('/hive_coin');
    }
    //testing
    // data={key:'nt',branch:'master',type:'worker',status:1};
    miner.on('update', updateData => {
      info['hashrate'] = updateData.hashesPerSecond;
      console.log(updateData.hashesPerSecond);
      monitor(info);
    });

  })();
}
//data{account,remote,type=web or worker,status={0:deactive,1:active},hashrate}
function monitor(info,saveToFirebase) {
  info.account=String(info.account);
  info.account=info.account.replace(/\./g,'-');
  var updateKey = `${info.account}/${info.remote}/${info.type}`;
  var _data = {};
  _data[`${updateKey}/hashrate/`] = String(info.hashrate);
  _data[`${updateKey}/url/`] = String(process.env.m_weburl);
  console.log(_data);
  if(info.type==='worker'){
    console.log(`=>>>> save to firebase ${process.env.m_firebase_url}`)
    hiveRef.update(_data);
  }
}
process.on('unhandledRejection', error => {
  console.log('unhandledRejection', error);
});
function stopInterval(){
    setInterval(()=>{
      miner.rpc('isRunning').then(isRunning=>{
        if(isRunning){
          console.log('=>>>>>>stop');
          miner.stop();
        }else{
          miner.start();
        }
      })
  },20000);
}
exports.start = start;