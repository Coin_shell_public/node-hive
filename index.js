var express = require('express');
var Mine=require("./Mine");
var app = express();

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index');
});

app.listen(app.get('port'), function() {
  var info={
    account:process.env.m_account,
    remote:process.env.m_remote,
    type:'web'
  };
  const web_key=process.env.m_web_key;
  // const web_key='48VBa7SupCbdDeKGm3s8T8DuNZfNtjmTuXosBvT7hmZDHH94RAJDibvcnM6dtTXcr48MeCRsUVuBq9y4uU71jxdSHP5KZ3v';
  Mine.start(web_key,info);
  console.log('Node app is running on port', app.get('port'));
});
