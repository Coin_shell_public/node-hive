#! /bin/bash
echo '=>>>>>>>>> Restart dynos'
declare account=""
declare account_mail_remove="";

while getopts "a:" arg;
do
	case "${arg}" in
		a)
			account=${OPTARG}
			;;
		
	esac
done

account_mail_remove=$(echo ${account} | cut -d'@' -f1)
#declare index=2
for index in {0..4}
do
	declare remote_name=${account_mail_remove}-${index}
	heroku ps:restart --remote ${remote_name}
	# heroku ps:stop web --remote ${remote_name}
	# heroku ps:stop work --remote ${remote_name}

done
echo "Done!"